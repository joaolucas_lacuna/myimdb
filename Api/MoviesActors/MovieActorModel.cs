﻿
using Api.Actors;
using Api.Movies;

namespace Api.MoviesActors
{
    public class MovieActorModel
    {
        public Guid Id { get; set; }
        public string Character { get; set; } = string.Empty;
        public MovieModel Movie { get; set; } = new();

        public ActorModel Actor { get; set; } = new();
    }
}
