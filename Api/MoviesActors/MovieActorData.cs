﻿using System.ComponentModel.DataAnnotations;

namespace Api.MoviesActors
{
    public class MovieActorData
    {
        [Required(ErrorMessage = "The character of the actor is required")]
        [MaxLength(100, ErrorMessage = "The character can't be greater than {1} characters")]
        public string Character { get; set; } = string.Empty;

        [Required(ErrorMessage = "The movieId of the movie is required")]
        public Guid? MovieId { get; set; }

        [Required(ErrorMessage = "The actorId of the actor is required")]
        public Guid? ActorId { get; set; }
    }
}
