﻿using Api.Genres;

namespace Api.Movies
{
    public class MovieModel
    {
        public Guid Id { get; set; }
        public int Rank { get; set; }
        public string Title { get; set; } = string.Empty;
        public int Year { get; set; }
        public string Storyline { get; set; } = string.Empty;
        public GenreModel? Genre { get; set; }
    }
}
