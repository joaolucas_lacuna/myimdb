﻿using System.ComponentModel.DataAnnotations;

namespace Api.Movies
{
    public class MovieData
    {
        [Required(ErrorMessage = "The title of the movie is required")]
        [MaxLength(100, ErrorMessage = "The title can't be greater than {1} characters")]
        public string Title { get; set; } = string.Empty;
        public int Rank { get; set; }

        public int Year { get; set; }

        public string Storyline { get; set; } = string.Empty;

        public string Genre { get; set; } = string.Empty;
    }
}
