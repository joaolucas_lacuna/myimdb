﻿using System.ComponentModel.DataAnnotations;

namespace Api.Genres
{
    // TODO : why this class is called GenreData?
    public class GenreData
    {
        [Required(ErrorMessage = "The name of the genre is required")]
        [MaxLength(100, ErrorMessage = "The name can't be greater than {1} characters")]
        public string Name { get; set; } = string.Empty;
    }
}
