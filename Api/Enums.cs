﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api
{
    public enum ErrorCodes
    {
        Unknown = 0,
        // movie enums
        MovieNotFound,
        MovieTitleAlreadyExists,

        // genre enums
        GenreNotFound,
        GenreNameAlreadyExists,

        // actor enums
        ActorNotFound,
        ActorNameAlreadyExists,

        // movieActor Enums
        MovieActorNotFound,
        MovieActorAlreadyExists
    }
}
