﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api
{
    public class ErrorModel
    {
        [JsonProperty("Code")]
        public string CodeStr { get; set; } = String.Empty;

        [JsonIgnore]
        public ErrorCodes Code
        {
            get
            {
                ErrorCodes res;
                try
                {
                    res = (ErrorCodes)Enum.Parse(typeof(ErrorCodes), CodeStr);
                }
                catch
                {
                    res = ErrorCodes.Unknown;
                }
                return res;
            }
            set
            {
                CodeStr = value.ToString();
            }
        }
        public string Message { get; set; } = String.Empty;

        public Dictionary<string, string> Details { get; set; } = new Dictionary<string, string>();

        public override string ToString()
        {
            var detailsString = String.Empty;

            if (detailsString is not null && Details.Count > 0)
            {
                detailsString = Environment.NewLine + String.Join(Environment.NewLine, Details);
            }

            return $"{CodeStr} - {Message}{Details}";
        }
    }
}
