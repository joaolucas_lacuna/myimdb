﻿
namespace Api.Actors
{
    public class ActorModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Birthplace { get; set; } = string.Empty;
    }
}
