﻿using System.ComponentModel.DataAnnotations;

namespace Api.Actors
{
    public class ActorData
    {
        [Required(ErrorMessage = "The name of the actor is required")]
        [MaxLength(100, ErrorMessage = "The name can't be greater than {1} characters")]
        public string Name { get; set; } = string.Empty;

        [MaxLength(100, ErrorMessage = "The birthplace can't be greater than {1} characters")]
        public string Birthplace { get; set; } = string.Empty;
    }
}
