﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Business.Repositories;
using MyImdb.Entities;

namespace MyImdb.Business.Services {
	public class MovieService {
		private readonly MovieRepository movieRepository;
		private readonly GenreRepository genreRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly AppDbContext dbContext;

		public MovieService(
			MovieRepository movieRepository,
			GenreRepository genreRepository,
			ExceptionBuilder exceptionBuilder,
			AppDbContext dbContext
		) {
			this.movieRepository = movieRepository;
			this.genreRepository = genreRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.dbContext = dbContext;
		}

		public async Task<Movie> CreateAsync(int rank, string title, int year, string storyline, string genre) {
			var existingMovie = await movieRepository.SelectByTitleAsync(title);

			if (existingMovie is not null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.MovieTitleAlreadyExists, new { title });
			}

			var getGenreByName = await genreRepository.SelectByNameAsync(genre);

			if (getGenreByName is null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.GenreNotFound, new { genre });
			}

			var movie = await movieRepository.CreateAsync(rank, title, year, storyline, getGenreByName!);

			await dbContext.SaveChangesAsync();

			return movie;
		}

		public async Task UpdateAsync(Movie movie, string title) {
			if (await dbContext.Movies!.AnyAsync(m => m.Title == title && m.Id != movie.Id)) {
				throw exceptionBuilder.Api(Api.ErrorCodes.MovieTitleAlreadyExists, new { title });
			}

			movie.Title = title;

			await dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(Movie movie) {

			dbContext.Remove(movie);

			await dbContext.SaveChangesAsync();
		}
	}
}
