﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Business.Repositories;
using MyImdb.Entities;

namespace MyImdb.Business.Services {
	public class ActorService {
		private readonly ActorRepository actorRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly AppDbContext dbContext;

		public ActorService(
			ActorRepository actorRepository,
			ExceptionBuilder exceptionBuilder,
			AppDbContext dbContext
			) {
			this.actorRepository = actorRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.dbContext = dbContext;
		}

		public async Task<Actor> CreateAsync(string name, string birthplace) {
			var existingActor = await actorRepository.SelectByNameAsync(name);

			if (existingActor != null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.ActorNameAlreadyExists, new { name });
			}

			var actor = await actorRepository.CreateAsync(name, birthplace);

			await dbContext.SaveChangesAsync();

			return actor;
		}
		public async Task UpdateAsync(Actor actor, string name, string birthplace) {
			if (await dbContext.Actors!.AnyAsync(a => a.Name == name && a.Id != actor.Id)) {
				throw exceptionBuilder.Api(Api.ErrorCodes.ActorNameAlreadyExists, new { name });
			}

			actor.Name = name;
			actor.Birthplace = !string.IsNullOrEmpty(birthplace) ? birthplace : actor.Birthplace;

			await dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(Actor actor) {
			dbContext.Remove(actor);

			await dbContext.SaveChangesAsync();
		}
	}
}
