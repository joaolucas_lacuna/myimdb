﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Business.Repositories;
using MyImdb.Entities;

namespace MyImdb.Business.Services
{
    public class GenreService
    {
        private readonly GenreRepository genreRepository;
        private readonly MovieRepository movieRepository;
        private readonly ExceptionBuilder exceptionBuilder;
        private readonly AppDbContext dbContext;

        public GenreService(
            GenreRepository genreRepository,
            MovieRepository movieRepository,
            ExceptionBuilder exceptionBuilder,
            AppDbContext dbContext
        )
        {
            this.genreRepository = genreRepository;
            this.movieRepository = movieRepository;
            this.exceptionBuilder = exceptionBuilder;
            this.dbContext = dbContext;
        }

        public async Task<Genre> CreateAsync(string name)
        {
            var existingGenre = await genreRepository.SelectByNameAsync(name);

            if (existingGenre is not null)
            {
                throw exceptionBuilder.Api(Api.ErrorCodes.GenreNameAlreadyExists, new { name });
            }

            var genre = await genreRepository.CreateAsync(name);

            await dbContext.SaveChangesAsync();

            return genre;
        }
        public async Task UpdateAsync(Genre genre, string name)
        {
            if (await dbContext.Genres!.AnyAsync(g => g.Name == name && g.Id != genre.Id))
            {
                throw exceptionBuilder.Api(Api.ErrorCodes.GenreNameAlreadyExists, new { name });
            }

            genre.Name = name;

            await dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Genre genre)
        {
            var movies = await movieRepository.SelectByGenreIdAsync(genre.Id);

            //delete all movies
            dbContext.RemoveRange(movies);

            dbContext.Remove(genre);

            await dbContext.SaveChangesAsync();
        }

    }
}
