﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Business.Repositories;
using MyImdb.Entities;

namespace MyImdb.Business.Services {
	public class MovieActorService {
		private readonly MovieActorRepository movieActorRepository;
		private readonly ActorRepository actorRepository;
		private readonly MovieRepository movieRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly AppDbContext dbContext;

		public MovieActorService(
			MovieActorRepository movieActorRepository,
			ActorRepository actorRepository,
			MovieRepository movieRepository,
			ExceptionBuilder exceptionBuilder,
			AppDbContext dbContext
			) {
			this.movieActorRepository = movieActorRepository;
			this.actorRepository = actorRepository;
			this.movieRepository = movieRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.dbContext = dbContext;
		}

		public async Task<MovieActor> CreateAsync(string character, Guid movieId, Guid actorId) {
			var movie = await movieRepository.SelectByIdAsync(movieId);

			if (movie == null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.MovieNotFound, new { movieId });
			}

			var actor = await actorRepository.SelectByIdAsync(actorId);

			if (actor == null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.ActorNotFound, new { actorId });
			}

			var movieActor = await movieActorRepository.CreateAsync(character, movieId, actorId);

			await dbContext.SaveChangesAsync();

			return movieActor;
		}

		public async Task UpdateAsync(MovieActor movieActor, string character) {
			if (await dbContext.MovieActors!.AnyAsync(ma => ma.Character == character && ma.Id == movieActor.Id)) {
				throw exceptionBuilder.Api(Api.ErrorCodes.MovieActorAlreadyExists, new { character });
			}

			movieActor.Character = character;

			await dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(MovieActor movieActor) {

			dbContext.Remove(movieActor);

			await dbContext.SaveChangesAsync();
		}
	}
}
