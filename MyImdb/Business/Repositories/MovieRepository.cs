﻿using Api.Genres;
using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;

namespace MyImdb.Business.Repositories {
	public class MovieRepository {
		private readonly AppDbContext dbContext;
		public MovieRepository(AppDbContext dbContext) {
			this.dbContext = dbContext;
		}

		// It's not necessary to get all the data from the table in the database
		//public async Task<List<Movie>> SelectAllAsync() => await dbContext.Movies!.ToListAsync();

		public async Task<List<Movie>> SelectTopNAsync(int n = 20) {
			var query = dbContext.Movies!.Include(m => m.Genre).OrderBy(m => m.Id).AsQueryable();
			query = query.Take(n);

			return await query.ToListAsync(); // database is called here
		}

		public async Task<Movie> CreateAsync(int rank, string title, int year, string storyline, Genre genre) {
			var movie = new Movie() {
				Id = Guid.NewGuid(),
				Rank = rank,
				Title = title,
				Year = year,
				Storyline = storyline,
				GenreId = genre.Id,
				// Genre = genre,
				CreationDate = DateTimeOffset.Now,
			};

			await dbContext.AddAsync(movie);

			return movie;
		}

		public async Task<Movie?> SelectByTitleAsync(string title) {
			return await dbContext.Movies!.Include(m => m.Genre).FirstOrDefaultAsync(m => m.Title == title);
		}

		public async Task<Movie?> SelectByIdAsync(Guid id) {
			return await dbContext.Movies!.Include(m => m.Genre).FirstOrDefaultAsync(m => m.Id == id);
		}

		public async Task<List<Movie>> SelectByGenreIdAsync(Guid id) {
			var query = dbContext.Movies!.Where(g => g.GenreId == id).AsQueryable();

			return await query.ToListAsync();
		}
	}
}
