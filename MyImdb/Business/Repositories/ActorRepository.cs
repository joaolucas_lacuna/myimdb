﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;

namespace MyImdb.Business.Repositories {
	public class ActorRepository {
		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public ActorRepository(
			AppDbContext dbContext,
			ExceptionBuilder exceptionBuilder) {

			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

		public async Task<Actor> CreateAsync(string name, string birthplace) {
			var actor = new Actor() {
				Id = Guid.NewGuid(),
				Name = name,
				Birthplace = birthplace
			};

			await dbContext.AddAsync(actor);

			return actor;
		}

		public async Task<Actor?> SelectByIdAsync(Guid id) {
			var actor = await dbContext.Actors!.FirstOrDefaultAsync(a => a.Id == id);

			return actor;
		}

		public async Task<Actor?> SelectByNameAsync(string name) {
			var actorByName = await dbContext.Actors!.FirstOrDefaultAsync(a => a.Name == name);

			return actorByName;
		}

		public async Task<List<Actor>> SelectTopNAsync(int n = 20) {
			var query = dbContext.Actors!.OrderBy(a => a.Id).AsQueryable();
			query = query.Take(n);

			return await query.ToListAsync(); // database is called here
		}
	}
}
