﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;

namespace MyImdb.Business.Repositories {
	public class MovieActorRepository {
		private readonly AppDbContext dbContext;

		public MovieActorRepository(AppDbContext dbContext) {
			this.dbContext = dbContext;
		}

		public async Task<MovieActor> CreateAsync(string character, Guid movieId, Guid actorId) {
			var movieActor = new MovieActor() {
				Id = Guid.NewGuid(),
				Character = character,
				// Just only necessary provide Actor or actorID
				ActorId = actorId,
				//Actor = actor,
				MovieId = movieId,
				//Movie = movie
			};

			await dbContext.AddAsync(movieActor);

			return movieActor;
		}

		public async Task<MovieActor?> SelectByIdAsync(Guid id) {
			return await dbContext.MovieActors!
				.Include(ma => ma.Actor)
				.Include(ma => ma.Movie)
				.ThenInclude(m => m.Genre)
				.FirstOrDefaultAsync(ma => ma.Id == id);
		}

		public async Task<List<MovieActor>> SelectTopNAsync(int n = 20) {
			var query = dbContext.MovieActors!
				.Include(ma => ma.Actor)
				.Include(ma => ma.Movie)
				.ThenInclude(m => m.Genre)
				.OrderBy(ma => ma.Id).AsQueryable();
			query = query.Take(n);

			return await query.ToListAsync(); // database is called here
		}
	}
}
