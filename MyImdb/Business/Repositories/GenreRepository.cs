﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;

namespace MyImdb.Business.Repositories {
	public class GenreRepository {
		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public GenreRepository(AppDbContext dbContext, ExceptionBuilder exceptionBuilder) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}
		public async Task<Genre> CreateAsync(string name) {
			var genre = new Genre() {
				Id = Guid.NewGuid(),
				Name = name
			};

			await dbContext.AddAsync(genre);

			return genre;
		}

		public async Task<Genre> SelectByIdAsync(Guid id) {
			var genreById = await dbContext.Genres!.FirstOrDefaultAsync(g => g.Id == id);

			if (genreById == null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.GenreNotFound, new { id });
			}

			return genreById;
		}

		public async Task<Genre?> SelectByNameAsync(string name) {
			var genreByName = await dbContext.Genres!.FirstOrDefaultAsync(g => g.Name == name);

			return genreByName;
		}

		public async Task<List<Genre>> SelectTopNAsync(int n = 20) {
			var query = dbContext.Genres!.OrderBy(m => m.Id).AsQueryable();
			query = query.Take(n);

			return await query.ToListAsync(); // database is called here
		}
	}
}
