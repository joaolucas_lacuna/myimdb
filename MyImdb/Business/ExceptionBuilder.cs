﻿using Api;

namespace MyImdb.Business {
	public class ExceptionBuilder {
		public ApiException Api(ErrorCodes code, object? details = null) {
			var message = String.Empty;

			switch (code) {
				case ErrorCodes.Unknown:
					message = "An Unknown error has ocurred";
					break;
				case ErrorCodes.MovieNotFound:
					message = "Movie not found";
					break;
				case ErrorCodes.MovieTitleAlreadyExists:
					message = "A movie with the provided title already exists";
					break;
				case ErrorCodes.GenreNameAlreadyExists:
					message = "A genre with the provided name already exists";
					break;
				case ErrorCodes.GenreNotFound:
					message = "Genre not found";
					break;
				case ErrorCodes.ActorNameAlreadyExists:
					message = "A actor with the provided name already exists";
					break;
				case ErrorCodes.ActorNotFound:
					message = "Actor not found";
					break;
				case ErrorCodes.MovieActorNotFound:
					message = "MovieActor not found";
					break;
				case ErrorCodes.MovieActorAlreadyExists:
					message = "A movieActor already exists with the given information";
					break;
				default:
					break;
			}

			return new ApiException(new ErrorModel { Code = code, Message = message, Details = getDetailsDictionary(details!)! });
		}

		private static Dictionary<string, string>? getDetailsDictionary(object details) {
			if (details is null) {
				return null;
			}
			var dic = new Dictionary<string, string>();

			foreach (var descriptor in details.GetType().GetProperties()) {
				dic[descriptor.Name] = descriptor.GetValue(details, null)?.ToString() ?? "null";
			}
			return dic;

		}
	}
}
