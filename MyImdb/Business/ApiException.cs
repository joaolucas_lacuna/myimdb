﻿using Api;

namespace MyImdb.Business
{
    public class ApiException : Exception
    {
        public ErrorModel Error { get; set; }

        public ApiException(ErrorModel error) : base(error.Message)
        {
            Error = error;
        }
    }
}
