﻿using Api.MoviesActors;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Business.Services;
using MyImdb.Services;

namespace MyImdb.Controllers {

	[ApiController]
	[Route("api/movie-actors")]
	public class MovieActorController : Controller {
		private readonly MovieActorService movieActorService;
		private readonly MovieActorRepository movieActorRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly ModelConverter mc;

		public MovieActorController(
			MovieActorService movieActorService,
			MovieActorRepository movieActorRepository,
			ExceptionBuilder exceptionBuilder,
			ModelConverter mc
			) {
			this.movieActorService = movieActorService;
			this.movieActorRepository = movieActorRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.mc = mc;
		}

		// POST: /api/movie-actors
		// Create a new movieActor
		[HttpPost]
		public async Task<MovieActorModel> Create(MovieActorData request) {
			var movieActor = await movieActorService.CreateAsync(request.Character, request.MovieId!.Value, request.ActorId!.Value);

			return mc.ToModel(movieActor);
		}

		// GET: /api/movie-actors
		// List `n` movieActors
		[HttpGet]
		public async Task<List<MovieActorModel>> List(int n = 20) {
			var movieActors = await movieActorRepository.SelectTopNAsync(n);

			return movieActors.ConvertAll(g => mc.ToModel(g));
		}

		// PUT: /api/movie-actors/{id}
		// Update a movie-actor by id
		[HttpPut("{id}")]
		public async Task<MovieActorModel> Update(Guid id, MovieActorData request) {
			var movieActor = await movieActorRepository.SelectByIdAsync(id);

			if (movieActor == null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.MovieNotFound, new { id });
			}

			await movieActorService.UpdateAsync(movieActor, request.Character);

			return mc.ToModel(movieActor);
		}

		// DELETE: /api/movie-actors/{id}
		// Delete a movieActor by id
		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var movieActor = await movieActorRepository.SelectByIdAsync(id);

			if (movieActor == null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.MovieActorNotFound, new { id });
			}

			await movieActorService.DeleteAsync(movieActor!);
		}
	}
}
