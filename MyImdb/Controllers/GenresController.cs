﻿using Api.Genres;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business.Repositories;
using MyImdb.Business.Services;
using MyImdb.Services;

namespace MyImdb.Controllers {
	[ApiController]
	[Route("api/genres")]
	public class GenresController : Controller {
		private readonly GenreRepository genreRepository;
		private readonly GenreService genreService;
		private readonly ModelConverter mc;

		public GenresController(
			GenreRepository genreRepository,
			GenreService genreService,
			ModelConverter mc
		) {
			this.genreRepository = genreRepository;
			this.genreService = genreService;
			this.mc = mc;
		}

		// GET: /api/genres/{id}
		// List genre by id
		[HttpGet("{id}")]
		public async Task<GenreModel> Get(Guid id) {
			var genre = await genreRepository.SelectByIdAsync(id);

			return mc.ToModel(genre!);
		}

		// GET: /api/genres
		// List `n` genres
		[HttpGet]
		public async Task<List<GenreModel>> List(int n = 20) {
			var genres = await genreRepository.SelectTopNAsync(n);

			return genres.ConvertAll(g => mc.ToModel(g));
		}

		// POST: /api/genres
		// Create a new genre
		[HttpPost]
		public async Task<GenreModel> Create(GenreData request) {
			var genre = await genreService.CreateAsync(request.Name);

			return mc.ToModel(genre);
		}

		// PUT: /api/genres/{id}
		// Update a genre by id
		[HttpPut("{id}")]
		public async Task<GenreModel> Update(Guid id, GenreData request) {
			var genre = await genreRepository.SelectByIdAsync(id);

			await genreService.UpdateAsync(genre!, request.Name);

			return mc.ToModel(genre!);
		}

		// DELETE: /api/genres/{id}
		// Delete a genre by id
		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var genre = await genreRepository.SelectByIdAsync(id);

			await genreService.DeleteAsync(genre!);
		}
	}
}
