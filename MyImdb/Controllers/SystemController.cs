﻿using Microsoft.AspNetCore.Mvc;
using System.Reflection;

namespace MyImdb.Controllers
{
    [ApiController]
    public class SystemController : Controller
    {
        [HttpGet("/api/system")]
        public object GetSystemInfo()
        {
            var assembly = Assembly.GetEntryAssembly();
            var assemblyName = assembly!.GetName();
            var version = assemblyName.Version!;

            return new
            {
                version = $"{version.Major}.{version.Minor}.{version.Build}",
            };
        }

    }
}
