﻿using Api.Actors;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Business.Services;
using MyImdb.Services;

namespace MyImdb.Controllers {

	[ApiController]
	[Route("api/actors")]
	public class ActorController : Controller {
		private readonly ActorRepository actorRepository;
		private readonly ActorService actorService;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly ModelConverter mc;

		public ActorController(
			ActorRepository actorRepository,
			ActorService actorService,
			ExceptionBuilder exceptionBuilder,
			ModelConverter mc
		) {
			this.actorRepository = actorRepository;
			this.actorService = actorService;
			this.exceptionBuilder = exceptionBuilder;
			this.mc = mc;
		}

		// GET: /api/actors/{id}
		// List actor by id
		[HttpGet("{id}")]
		public async Task<ActorModel> Get(Guid id) {
			var actor = await actorRepository.SelectByIdAsync(id);

			if (actor == null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.ActorNotFound, new { id });
			}

			return mc.ToModel(actor);
		}

		// GET: /api/actors
		// List `n` actors
		[HttpGet]
		public async Task<List<ActorModel>> List(int n = 20) {
			var actors = await actorRepository.SelectTopNAsync(n);

			return actors.ConvertAll(g => mc.ToModel(g));
		}

		// POST: /api/actors
		// Create a new actor
		[HttpPost]
		public async Task<ActorModel> Create(ActorData request) {
			var actor = await actorService.CreateAsync(request.Name, request.Birthplace);

			return mc.ToModel(actor);
		}

		// PUT: /api/actors/{id}
		// Update a actor by id
		[HttpPut("{id}")]
		public async Task<ActorModel> Update(Guid id, ActorData request) {
			var actor = await actorRepository.SelectByIdAsync(id);

			if (actor == null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.ActorNotFound, new { id });
			}

			await actorService.UpdateAsync(actor, request.Name, request.Birthplace);

			return mc.ToModel(actor);
		}

		// DELETE: /api/actors/{id}
		// Delete a actor by id
		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var actor = await actorRepository.SelectByIdAsync(id);

			if (actor == null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.ActorNotFound, new { id });
			}

			await actorService.DeleteAsync(actor);
		}
	}
}
