﻿using Api.Genres;
using Api.Movies;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Business.Services;
using MyImdb.Entities;
using MyImdb.Services;
using MyImdb.ViewModels;
using System.Diagnostics.CodeAnalysis;

namespace MyImdb.Controllers {
	[ApiController]
	[Route("api/movies")]
	public class MovieController : Controller {
		private readonly MovieRepository movieRepository;
		private readonly MovieService movieService;
		private readonly GenreRepository genreRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly ModelConverter mc;

		public MovieController(
			MovieRepository movieRepository,
			MovieService movieService,
			GenreRepository genreRepository,
			ExceptionBuilder exceptionBuilder,
			ModelConverter mc
		) {
			this.movieRepository = movieRepository;
			this.movieService = movieService;
			this.genreRepository = genreRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.mc = mc;
		}

		// GET: /api/movies/
		// List all movies
		[HttpGet]
		public async Task<List<MovieModel>> List(int n = 20) {
			// TODO: pagination API, name filter

			var movies = await movieRepository.SelectTopNAsync(n);

			return movies.ConvertAll(m => mc.ToModel(m));
		}

		// GET: /api/movies/
		// Create a movie
		[HttpPost]
		public async Task<MovieModel> Create(MovieData request) {

			var movie = await movieService.CreateAsync(request.Rank, request.Title, request.Year, request.Storyline, request.Genre);

			return mc.ToModel(movie);
		}

		// GET: /api/movies/{id}
		// List movie by id
		[HttpGet("{id}")]
		public async Task<MovieModel> Get(Guid id) {
			var movie = await movieRepository.SelectByIdAsync(id);

			return mc.ToModel(movie!);
		}

		// PUT: /api/movies/{id}
		// Update a movie by id
		[HttpPut("{id}")]
		public async Task<MovieModel> Update(Guid id, MovieData request) {
			var movie = await movieRepository.SelectByIdAsync(id);

			if (movie == null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.MovieNotFound, new { id });
			}

			await movieService.UpdateAsync(movie, request.Title);

			return mc.ToModel(movie);
		}

		// DELETE: /api/genres/{id}
		// Delete a genre by id
		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var movie = await movieRepository.SelectByIdAsync(id);

			if (movie == null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.MovieNotFound, new { id });
			}

			await movieService.DeleteAsync(movie);
		}
	}
}
