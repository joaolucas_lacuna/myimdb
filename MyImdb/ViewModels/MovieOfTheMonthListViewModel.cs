namespace MyImdb.ViewModels
{
    public class MovieOfTheMonthListViewModel
    {
        public int Rank { get; set; }
        public string Title { get; set; } = String.Empty;
        public string Director { get; set; } = String.Empty;
        public string StoryLine { get; set; } = String.Empty;
    }
}
