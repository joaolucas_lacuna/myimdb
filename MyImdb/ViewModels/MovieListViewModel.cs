﻿namespace MyImdb.ViewModels
{
    public class MovieListViewModel
    {
        public int Rank { get; set; }
        public string Title { get; set; } = String.Empty;
        public int Year { get; set; }
    }
}
