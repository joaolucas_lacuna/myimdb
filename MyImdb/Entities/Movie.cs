﻿using MyImdb.Business;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyImdb.Entities {
	public class Movie {
		public Guid Id { get; set; }
		public int Rank { get; set; }

		[MaxLength(100)]
		[Required]
		public string Title { get; set; } = string.Empty;
		public int Year { get; set; }

		[MaxLength(200)]
		public string Storyline { get; set; } = String.Empty;
		public DateTime CreationDateUtc { get; set; }

		public Guid GenreId { get; set; }
		public Genre Genre { get; set; } = new();

		public List<MovieActor> MovieActors { get; } = new();

		[NotMapped]
		public DateTimeOffset CreationDate {
			get {
				return new DateTimeOffset(CreationDateUtc, TimeSpan.Zero);
			}
			set {
				CreationDateUtc = value.UtcDateTime;
			}
		}
	}
}
