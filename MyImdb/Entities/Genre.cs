﻿using MyImdb.Entities;
using System.ComponentModel.DataAnnotations;

namespace MyImdb.Business {
	public class Genre {
		public Guid Id { get; set; }

		[MaxLength(100)]
		[Required]
		public string Name { get; set; } = string.Empty;

		public List<Movie> Movies { get; } = new();
	}
}
