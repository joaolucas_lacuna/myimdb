using Microsoft.EntityFrameworkCore;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Business.Services;
using MyImdb.Configuration;
using MyImdb.Entities;
using MyImdb.Services;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Logging.AddSerilog(
	new LoggerConfiguration()
		.ReadFrom
		.Configuration(builder.Configuration, "Serilog")
		.CreateLogger()
);

// Add services to the container.
builder.Services.AddControllersWithViews(o => {
	o.Filters.Add<ValidateModelStateAttribute>();
	o.Filters.Add<HandleExceptionFilter>();
});

builder.Services.AddDbContext<AppDbContext>(options =>
	options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")
));

builder.Services.AddScoped<MovieRepository>();
builder.Services.AddScoped<GenreRepository>();
builder.Services.AddScoped<ActorRepository>();
builder.Services.AddScoped<MovieActorRepository>();

builder.Services.AddScoped<MovieService>();
builder.Services.AddScoped<GenreService>();
builder.Services.AddScoped<ActorService>();
builder.Services.AddScoped<MovieActorService>();

builder.Services.AddScoped<ExceptionBuilder>();
builder.Services.AddScoped<ModelConverter>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment()) {
	app.UseExceptionHandler("/Home/Error");
	// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
	app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

//app.MapControllerRoute(
//    name: "default",
//    pattern: "{controller=Home}/{action=Index}");

app.MapControllers();

updateDatabase(app);

app.Run();

#region DATABASE
void updateDatabase(WebApplication host) {
	using (var scope = host.Services.CreateScope()) {
		var services = scope.ServiceProvider;
		try {
			var context = services.GetRequiredService<AppDbContext>();
			context.Database.Migrate();
		} catch (Exception ex) {
			var logger = services.GetRequiredService<ILogger<Program>>();
			logger.LogError(ex, "An error occurred migrating the DB.");
		}
	}
}
#endregion
