﻿using Api.Actors;
using Api.Genres;
using Api.Movies;
using Api.MoviesActors;
using MyImdb.Business;
using MyImdb.Entities;

namespace MyImdb.Services {
	// ModelConverter's job is to convert entities into API models
	public class ModelConverter {
		public ModelConverter() { }

		public GenreModel ToModel(Genre genre) {
			return new GenreModel() {
				Id = genre.Id,
				Name = genre.Name
			};
		}

		public MovieModel ToModel(Movie movie) {
			return new MovieModel() {
				Id = movie.Id,
				Rank = movie.Rank,
				Title = movie.Title,
				Year = movie.Year,
				Storyline = movie.Storyline,
				Genre = ToModel(movie.Genre!)
			};
		}

		public ActorModel ToModel(Actor actor) {
			return new ActorModel() {
				Id = actor.Id,
				Name = actor.Name,
				Birthplace = actor.Birthplace
			};
		}

		public MovieActorModel ToModel(MovieActor movieActor) {
			return new MovieActorModel() {
				Id = movieActor.Id,
				Character = movieActor.Character,
				Movie = ToModel(movieActor.Movie),
				Actor = ToModel(movieActor.Actor)
			};
		}
	}
}
